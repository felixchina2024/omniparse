# OmniParse

![OmniParse](https://raw.githubusercontent.com/adithya-s-k/omniparse/main/docs/assets/hero_image.png)
[![GitHub Stars](https://img.shields.io/github/stars/adithya-s-k/omniparse?style=social)](https://github.com/adithya-s-k/omniparse/stargazers)
[![GitHub Forks](https://img.shields.io/github/forks/adithya-s-k/omniparse?style=social)](https://github.com/adithya-s-k/omniparse/network/members)
[![GitHub Issues](https://img.shields.io/github/issues/adithya-s-k/omniparse)](https://github.com/adithya-s-k/omniparse/issues)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/adithya-s-k/omniparse)](https://github.com/adithya-s-k/omniparse/pulls)
[![License](https://img.shields.io/github/license/adithya-s-k/omniparse)](https://github.com/adithya-s-k/omniparse/blob/main/LICENSE)


功能✅ 完全本地，无需外部

API✅ 适合 T4 GPU✅ 支持 ~20 种文件类型✅ 将文档、多媒体和网页转换为高质量的结构化

markdown✅ 表格提取、图像提取/字幕、音频/视频转录、网页抓取✅ 使用 Docker 和 Skypilot

轻松部署✅ Colab 友好✅ 由 Gradio 提供支持的交互式 UI \

问题陈述 处理数据很有挑战性，因为数据有各种形状和大小。OmniParse 旨在成为一个摄取/解析平台，您可以在其中摄取任何类型的数据，例如文档、图像、音频、视频和 Web 内容，并获得最结构化、可操作且对 GenAI (LLM) 友好的输出。 安装说明：服务器仅适用于基于 Linux 的系统。这是由于某些依赖项和系统特定配置与 Windows 或 macOS 不兼容。 要安装 OmniParse，您可以使用 pip：
git clone https://github.com/adithya-s-k/omniparse
cd omniparse

创建虚拟环境：
conda create --name omniparse-venv python=3.10
conda activate omniparse-venv

安装依赖项：
poetry install
# or
pip install -e .

将 OmniParse 与 Docker 结合使用，执行以下命令：从 Docker Hub 中提取 OmniParse API Docker 镜像：运行 Docker 容器，公开端口 8000：Docker 镜像：
docker pull savatar101/omniparse:0.1
# if you are running on a gpu
docker run --gpus all -p 8000:8000 savatar101/omniparse:0.1
# else
docker run -p 8000:8000 savatar101/omniparse:0.1

或者，如果您更喜欢在本地构建 Docker 映像：然后，按如下方式运行 Docker 容器：
docker build -t omniparse .
# if you are running on a gpu
docker run --gpus all -p 8000:8000 omniparse
# else
docker run -p 8000:8000 omniparse

使用运行服务器：
python server.py --host 0.0.0.0 --port 8000 --documents --media --web

文档：加载所有帮助您解析和提取文档的模型（Surya OCR 系列模型和 Florence-2）。--media：加载 Whisper 模型来转录音频和视频文件。--web：设置 selenium 爬虫。
支持的数据类型：

Type	Supported Extensions
Documents	.doc, .docx, .pdf, .ppt, .pptx
Images	.png, .jpg, .jpeg, .tiff, .bmp, .heic
Video	.mp4, .mkv, .avi, .mov
Audio	.mp3, .wav, .aac
Web	dynamic webpages, http://.com
API终点
即将推出/路线图
🦙 LlamaIndex|Langchain|Haystack集成即将推出📚 批量处理数据⭐ 基于指定Schema的动态分块和结构化数据提取
🛠️ 一个神奇的API：只需在你的文件提示中输入你想要的内容，我们就会处理剩下的内容
🔧 动态模型选择和对外部API的支持
📄 一次处理多个文件的批处理
📦 新的开源模型取代Surya OCR和Marker

最终目标：将当前使用的所有不同模型替换为单个多模型模型，以解析任何类型的数据并获得所需的数据。

许可证
OmniParse是根据GPL-3.0许可证获得许可的。有关详细信息，请参阅许可证。该项目使用Marker under-The-hood，它有一个需要遵守的商业许可证。以下是详细信息：

商业用途
Marker和Surya OCR模型设计为尽可能广泛地使用，同时仍为开发和培训成本提供资金。研究和个人使用总是允许的，但商业使用有一些限制。这些型号的重量是根据cc-by-nc-sa-4.0许可的。但是，任何在12个月内收入低于500万美元且低于
筹集了500万终身风险投资/天使基金。要取消GPL许可证要求（双重许可证）和/或在商业上使用超过收入限制的权重，请查看提供的选项。请参阅Marker了解有关模型权重许可证的更多信息

鸣谢
该项目建立在Vik Paruchuri创建的卓越Marker项目的基础上。我们对该项目提供的灵感和基础表示感谢。特别感谢Surya OCR和Texify在该项目中广泛使用的OCR模型，以及Crawl4AI的贡献。

正在使用的型号：

Surya OCR、检测、布局、排序和Texify
Florence-2基地
Whisper Small